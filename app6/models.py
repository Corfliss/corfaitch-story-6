from django.db import models

class PesertaBelajar(models.Model):
    nama_peserta_belajar = models.CharField(max_length=30)

class PesertaMain(models.Model):
    nama_peserta_main = models.CharField(max_length=30)

class PesertaTidur(models.Model):
    nama_peserta_tidur = models.CharField(max_length=30)
# Create your models here.

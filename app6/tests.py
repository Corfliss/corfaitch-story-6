from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from http import HTTPStatus
from .models import PesertaBelajar, PesertaMain, PesertaTidur

# Create your tests here.
class Story6Test(TestCase):
    def test_story_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_story_6_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_story_6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story_6_filling_the_belajar_form(self):
        response = self.client.post(
            "/add_peserta_belajar", data={"nama_peserta_belajar": "nama orang 1"}
        )
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
    
    def test_story_6_filling_the_main_form(self):
        response = self.client.post(
            "/add_peserta_main", data={"nama_peserta_main": "nama orang 2"}
        )
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
    
    def test_story_6_filling_the_tidur_form(self):
        response = self.client.post(
            "/add_peserta_tidur", data={"nama_peserta_tidur": "nama orang 3"}
        )
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_story_6_delete_the_belajar_form(self):
        belajar = PesertaBelajar.objects.create(nama_peserta_belajar="No1")
        belajar_id = PesertaBelajar.objects.get(nama_peserta_belajar='No1') 
        delete_id = belajar_id.id  
        response = self.client.get(
            "/delete_belajar/" + str(delete_id)
        )
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_story_6_delete_the_main_form(self):
        main = PesertaMain.objects.create(nama_peserta_main="No2")
        main_id = PesertaMain.objects.get(nama_peserta_main='No2')
        delete_id = main_id.id
        response = self.client.get(
            "/delete_main/"+ str(delete_id)
        )
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_story_6_delete_the_tidur_form(self):
        tidur = PesertaTidur.objects.create(nama_peserta_tidur="No3")
        tidur_id = PesertaTidur.objects.get(nama_peserta_tidur='No3')
        delete_id = tidur_id.id
        response = self.client.get(
            "/delete_tidur/"+ str(delete_id)
        )
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
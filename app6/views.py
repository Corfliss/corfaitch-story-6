from django.shortcuts import render, redirect
from .models import PesertaBelajar, PesertaMain, PesertaTidur
from .forms import FormPesertaBelajar, FormPesertaMain, FormPesertaTidur

# Create your views here.
def index(request):
    response = {
        'form_peserta_belajar' : FormPesertaBelajar,
        'list_peserta_belajar' : PesertaBelajar.objects.all(),
        'form_peserta_main' : FormPesertaMain,
        'list_peserta_main' : PesertaMain.objects.all(),
        'form_peserta_tidur' : FormPesertaTidur,
        'list_peserta_tidur' : PesertaTidur.objects.all(),
    }
    return render(request, 'index.html', response)

def add_peserta_belajar(request):
    form = FormPesertaBelajar(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        add_peserta_belajar = PesertaBelajar.objects.create(nama_peserta_belajar=request.POST['nama_peserta_belajar'])
    return redirect('app6:index')

def add_peserta_main(request):
    form = FormPesertaMain(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        add_peserta_main = PesertaMain.objects.create(nama_peserta_main=request.POST['nama_peserta_main'])
    return redirect('app6:index')

def add_peserta_tidur(request):
    form = FormPesertaTidur(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        add_peserta_tidur = PesertaTidur.objects.create(nama_peserta_tidur=request.POST['nama_peserta_tidur'])
    return redirect('app6:index')

def delete_belajar(request, delete_id):
   PesertaBelajar.objects.filter(id=delete_id).delete()
   return redirect('app6:index')

def delete_main(request, delete_id):
   PesertaMain.objects.filter(id=delete_id).delete()
   return redirect('app6:index')

def delete_tidur(request, delete_id):
   PesertaTidur.objects.filter(id=delete_id).delete()
   return redirect('app6:index')
